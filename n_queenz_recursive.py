# N Queen problem
# Recursive solution

# number of queens to start with 
queenz = 4

# holds current testing data 
current_solution = [0 for x in range(queenz)]

# number of solutions found 
solutions = []

def safe(test_row, test_column):
    # No need to check for row 0
    if test_row == 0:
        return True

    for row in range(0, test_row):

        # check vertical
        if test_column == current_solution[row]:
            return False

        # diagonal
        if abs(test_row - row) == abs(test_column - current_solution[row]):
            return False

    # no attacks found 
    return True 

def queen_move(row):
    global current_solution, solutions, queenz

    for column in range(queenz):
        if not safe(row, column):
            continue
        else:
            current_solution[row] = column
            if row == (queenz - 1): 
                # last row 
                solutions.append(current_solution.copy())
            else:
                queen_move(row + 1)


queen_move(0)

print(len(solutions), " solutions found")
for solution in solutions:
    print(solution) 